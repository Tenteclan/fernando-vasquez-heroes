import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    let heroes = [
        {id:11, name:'Feca', image:'../media/personajes/feca.png', Bimage:'../media/fondoPersonajes/bFeca.jpg'},
        {id:12, name:'Yopuka', image:'../media/personajes/Yopuka.png', Bimage:'../media/fondoPersonajes/bYopuka.jpg'},
        {id:13, name:'Pandawa', image:'../media/personajes/pandawa.png', Bimage:'../media/fondoPersonajes/bPandawa.jpg'},
        {id:14, name:'Sacrogrito', image:'../media/personajes/sacrogrito.png', Bimage:'../media/fondoPersonajes/bSacro.jpg'},
        {id:15, name:'Sram', image:'../media/personajes/sram.png', Bimage:'../media/fondoPersonajes/bSram.jpg'},
        {id:16, name:'Anutrof', image:'../media/personajes/anutrof.png', Bimage:'../media/fondoPersonajes/bAnutrof.jpg'},
        {id:17, name:'Sadida', image:'../media/personajes/sadida.png', Bimage:'../media/fondoPersonajes/bSadida.jpg'},
        {id:18, name:'Zurcarak', image:'../media/personajes/zurcarak.png', Bimage:'../media/fondoPersonajes/bZurcarak.jpg'},
        {id:19, name:'Xelor', image:'../media/personajes/xelor.png', Bimage:'../media/fondoPersonajes/bXelor.jpg'},
        {id:20, name:'Timador', image:'../media/personajes/Tymador.png', Bimage:'../media/fondoPersonajes/bTymador.jpg'},
        {id:21, name:'Ocra', image:'../media/personajes/ocra.png', Bimage:'../media/fondoPersonajes/bOcra.jpg'},
        {id:22, name:'Aniripsa', image:'../media/personajes/aniripsa.png', Bimage:'../media/fondoPersonajes/bAniripsa.jpg'},
        {id:23, name:'Osamodas', image:'../media/personajes/osamodas.png', Bimage:'../media/fondoPersonajes/bOsamodas.jpg'},
        {id:24, name:'Zobal', image:'../media/personajes/zobal.png', Bimage:'../media/fondoPersonajes/bZobal.jpg'}
    ];
    return {heroes};
  }
}